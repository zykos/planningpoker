defmodule PlanningPoker do
  use Application

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    children = [
      # Start the endpoint when the application starts
      supervisor(PlanningPoker.Endpoint, []),
      # Here you could define other workers and supervisors as children
      worker(PlanningPoker.RoomRegistry, []),
      supervisor(PlanningPoker.RoomSupervisor, []),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :rest_for_one, name: PlanningPoker.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    PlanningPoker.Endpoint.config_change(changed, removed)
    :ok
  end
end
