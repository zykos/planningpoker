defmodule PlanningPoker.RoomSupervisor do
  use Supervisor

  @name PlanningPoker.RoomSupervisor

  def start_link do
    Supervisor.start_link(__MODULE__, :ok, name: @name)
  end

  def start_room do
    Supervisor.start_child(@name, [])
  end

  def init(:ok) do
    children = [
      worker(PlanningPoker.Room, [], restart: :temporary)
    ]

    supervise(children, strategy: :simple_one_for_one)
  end
end