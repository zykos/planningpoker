defmodule PlanningPoker.RoomRegistry do
  alias PlanningPoker.Room
  alias PlanningPoker.RoomSupervisor
  use GenServer

  @name PlanningPoker.RoomRegistry

  # Client API

  def start_link do
    GenServer.start_link(__MODULE__, :ok, name: @name)
  end

  def stop do
    GenServer.stop(@name)
  end

  def list do
    GenServer.call(@name, :list)
  end

  def create(room_name) do
    GenServer.call(@name, {:create, room_name})
  end

  def destroy(room_name) do
    GenServer.call(@name, {:destroy, room_name})
  end

  def lookup(room_name) do
    GenServer.call(@name, {:lookup, room_name})
  end

  def exists?(room_name) do
    GenServer.call(@name, {:exists, room_name})
  end


  def members(room_name) do
    GenServer.call(@name, {:members, room_name})
  end

  def votes(room_name) do
    GenServer.call(@name, {:votes, room_name})
  end

  def get_estimate(room_name) do
    GenServer.call(@name, {:get_estimate, room_name})
  end

  def join(room_name, user) do
    GenServer.call(@name, {:join, room_name, user})
  end

  def leave(room_name, user) do
    GenServer.call(@name, {:leave, room_name, user})
  end

  def member?(room_name, user) do
    GenServer.call(@name, {:member, room_name, user})
  end


  def vote(room_name, user, vote) do
    GenServer.call(@name, {:vote, room_name, user, vote})
  end

  def get_vote(room_name, user) do
    GenServer.call(@name, {:get_vote, room_name, user})
  end

  # Server Callbacks

  def init(:ok) do
    rooms = %{}
    refs = %{}
    {:ok, {rooms, refs}}
  end

  def handle_call(:list, _from, {rooms, refs}) do
    {:reply, Map.keys(rooms), {rooms, refs}}
  end

  def handle_call({:create, room_name}, _from, state) do
    {:ok, room, newstate} = create_room(state, room_name)
    {:reply, {:ok, room}, newstate}
  end

  def handle_call({:destroy, room_name}, _from, state) do
    result = destroy_room(state, room_name)
    {:reply, result, state}
  end

  def handle_call({:lookup, room_name}, _from, state) do
    {:reply, lookup_room(state, room_name), state}
  end

  def handle_call({:exists, room_name}, _from, state) do
    {:reply, room_exists?(state, room_name), state}
  end

  def handle_call({:members, room_name}, _from, state) do
    room_pid = lookup_room(state, room_name)
    {:reply, Room.members(room_pid), state}
  end

  def handle_call({:votes, room_name}, _from, state) do
    room_pid = lookup_room(state, room_name)
    {:reply, Room.votes(room_pid), state}
  end

  def handle_call({:get_estimate, room_name}, _from, state) do
    room_pid = lookup_room(state, room_name)
    {:reply, Room.get_estimate(room_pid), state}
  end

  def handle_call({:join, room_name, user}, _from, state) do
    {_, room_pid, newstate} = get_or_create_room(state, room_name)
    {:reply, Room.join(room_pid, user), newstate}
  end

  def handle_call({:leave, room_name, user}, _from, state) do
    room_pid = lookup_room(state, room_name)
    {:reply, Room.leave(room_pid, user), state}
  end

  def handle_call({:member, room_name, user}, _from, state) do
    room_pid = lookup_room(state, room_name)
    {:reply, Room.member?(room_pid, user), state}
  end

  def handle_call({:vote, room_name, user, vote}, _from, state) do
    {_, room_pid, newstate} = get_or_create_room(state, room_name)
    {:reply, Room.vote(room_pid, user, vote), newstate}
  end

  def handle_call({:get_vote, room_name, user}, _from, state) do
    room_pid = lookup_room(state, room_name)
    {:reply, Room.get_vote(room_pid, user), state}
  end

  def handle_info({:DOWN, ref, :process, _pid, _reason}, {rooms, refs}) do
    {name, refs} = Map.pop(refs, ref)
    rooms = Map.delete(rooms, name)
    {:noreply, {rooms, refs}}
  end

  # Private functions

  defp room_exists?({rooms, _refs}, room_name) do
    Map.has_key?(rooms, room_name)
  end

  defp lookup_room({rooms, _refs}, room_name) do
    Map.fetch!(rooms, room_name)
  end

  defp create_room({rooms, refs}, room_name) do
    {:ok, pid} = RoomSupervisor.start_room
    ref = Process.monitor(pid)
    refs = Map.put(refs, ref, room_name)
    rooms = Map.put(rooms, room_name, pid)
    {:ok, pid, {rooms, refs}}
  end

  defp get_or_create_room(state, room_name) do
    if room_exists?(state, room_name) do
      {:exists, lookup_room(state, room_name), state}
    else
      {:ok, pid, newstate} = create_room(state, room_name)
      {:created, pid, newstate}
    end
  end

  defp destroy_room({rooms, _refs}, room_name) do
    case Map.get(rooms, room_name) do
      nil -> :error
      room_pid -> Room.stop(room_pid)
    end
  end
end