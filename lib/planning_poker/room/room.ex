defmodule PlanningPoker.Room do
  alias PlanningPoker.Room, as: Room

  def start_link do
    Agent.start_link(fn -> %{} end)
  end

  def stop(room) do
    Agent.stop(room)
  end

  def members(room) do
    Agent.get(room, fn users ->
      Map.keys(users)
    end)
  end

  def votes(room) do
    Agent.get(room, fn users ->
      Map.values(users)
    end)
  end

  def empty?(room) do
    Agent.get(room, fn users ->
      users == %{}
    end)
  end

  def join(room, user) do
    Agent.update(room, fn users ->
      Map.put_new(users, user, nil)
    end)
  end

  def leave(room, user) do
    Agent.update(room, fn users ->
      Map.delete(users, user)
    end)
    if Room.empty?(room), do: stop(room)
  end

  def member?(room, user) do
    Agent.get(room, fn users ->
      Map.has_key?(users, user)
    end)
  end

  def vote(room, user, vote) do
    Agent.update(room, fn users ->
      Map.put(users, user, vote)
    end)
  end

  def get_vote(room, user) do
    Agent.get(room, fn users ->
      Map.get(users, user)
    end)
  end

  def get_estimate(room) do
    {sum, length} = Room.votes(room)
                 |> Enum.reduce({0,0}, &aggregate_votes/2)
    fibonaccize(sum / length, 0, 1)
  end


  # Private functions

  defp aggregate_votes(vote, {sum, length}) when is_integer(vote) do
    {sum + vote, length + 1}
  end

  defp aggregate_votes(vote,  acc) do
    case Integer.parse(vote) do
      {integer, _} -> aggregate_votes(integer, acc)
      :error -> acc
    end
  end

  defp fibonaccize(number, fib1, fib2) do
    cond do
      number <= fib2 -> fib2
      true -> fibonaccize(number, fib2, fib1 + fib2)
    end
  end
end