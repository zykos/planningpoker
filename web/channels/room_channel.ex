defmodule PlanningPoker.RoomChannel do
  use Phoenix.Channel

  defp compute_average(votes) do
    {sum, length} = List.foldl(votes, {0,0}, fn (v, {s, l} = acc) ->
      case Integer.parse(v) do
        {integer, _} -> {s + integer, l + 1}
        _ -> acc
      end
    end)
    sum / length
  end

  def join("rooms:" <> room, auth_msg, socket) do
    {:ok, socket}
  end

  def handle_in("new_msg", %{"body" => body}, socket) do
    broadcast! socket, "new_msg", %{body: body}
    {:noreply, socket}
  end

  def handle_in("card_select", %{"card" => card}, socket) do
    user = socket.assigns[:user]
    broadcast! socket, "new_msg", %{body: "#{user} selected #{card}"}
    votes = Map.update(socket.assigns[:votes], user, card, fn _ -> card end)
    {:noreply, assign(socket, :votes, votes)}
  end

  def handle_in("flip", _payload, socket) do
    vote = socket.assigns[:votes] |> Map.values |> compute_average
    broadcast! socket, "new_msg", %{body: "The story estimate is #{vote}"}
    {:noreply, socket}
  end

  def handle_out("new_msg", payload, socket) do
    push socket, "new_msg", payload
    {:noreply, socket}
  end
end