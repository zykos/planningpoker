defmodule PlanningPoker.PageController do
  use PlanningPoker.Web, :controller

  def index(conn, _params) do
    render conn, "index.html", rooms: ["lobby", "foo"]
  end

  def join(conn, %{"user" => user, "old_room" => room, "action" => "join"}) do
    redirect assign(conn, :user, user), to: "/room/#{room}/as/#{user}"
  end

  def join(conn, %{"user" => user, "new_room" => room, "action" => "create"}) do
    redirect assign(conn, :user, user), to: "/room/#{room}/as/#{user}"
  end

  def room(conn, %{"room" => room, "user" => user}) do
    cards = [0, 1, 2, 3, 5, 8, 13, "?", "∞", "☕"]
    render conn, "room.html", room: room, user: user, cards: cards
  end
end
