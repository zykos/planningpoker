defmodule PlanningPoker.PageControllerTest do
  use PlanningPoker.ConnCase

  test "GET /", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "Planning Poker"
  end
end
