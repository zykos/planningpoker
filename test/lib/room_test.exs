defmodule PlanningPoker.RoomTest do
  alias PlanningPoker.RoomRegistry
  use ExUnit.Case

  test "creating and destroying a room" do
    {:ok, _room_pid} = RoomRegistry.create "myroom"
    assert RoomRegistry.exists? "myroom"

    :ok = RoomRegistry.destroy "myroom"
    assert not RoomRegistry.exists? "myroom"
  end

  test "joining and leaving a room" do
    RoomRegistry.join "myroom", "Alice"
    RoomRegistry.join "myroom", "Bob"

    assert RoomRegistry.list == ["myroom"]

    assert RoomRegistry.member? "myroom", "Bob"
    assert RoomRegistry.member? "myroom", "Alice"

    RoomRegistry.leave "myroom", "Alice"
    assert not RoomRegistry.member? "myroom", "Alice"
    RoomRegistry.leave "myroom", "Bob"
    assert not RoomRegistry.exists? "myroom"
  end

  test "voting in a room" do
    RoomRegistry.vote "myroom", "Alice", "1"
    RoomRegistry.vote "myroom", "Bob", "5"
    RoomRegistry.vote "myroom", "Charlie", "?"
    RoomRegistry.vote "myroom", "David", 5

    assert "1" == RoomRegistry.get_vote "myroom", "Alice"
    assert "5" == RoomRegistry.get_vote "myroom", "Bob"

    assert 5 == RoomRegistry.get_estimate "myroom"

    assert ["1", "5", "?", 5] == RoomRegistry.votes "myroom"
    assert ["Alice", "Bob", "Charlie", "David"] == RoomRegistry.members "myroom"

    RoomRegistry.destroy "myroom"
  end

  test "joining and voting multiple times" do
    RoomRegistry.join "myroom", "Alice"
    RoomRegistry.join "myroom", "Alice"

    assert ["Alice"] == RoomRegistry.members "myroom"

    RoomRegistry.vote "myroom", "Bob", 3
    RoomRegistry.vote "myroom", "Bob", 5

    assert 5 == RoomRegistry.get_vote "myroom", "Bob"
    assert [nil, 5] == RoomRegistry.votes "myroom"
  end
end